const {
  movie,
  review
} = require('../models')

class MovieController {

  async getAll(req, res) {
    const count = await movie.countDocuments();
    const {
      page = 1, limit = 10
    } = req.query;

    movie.find({}).limit(limit * 1).skip((page - 1) * limit).then(result => {
      res.json({
        status: "Success",
        data: result,
        totalPages: Math.ceil(count / limit),
        currentPage: page,
        totalData: count
      })
    })
  }


  async getOne(req, res) {
    movie.findOne({
      _id: req.params.id
    }).then(result => {
      res.json({
        status: "Success",
        data: result
      })
    })
  }

  async getByTitle(req, res) {
    movie.find({
      title: {
        $regex: '.*' + req.params.title + '.*'
      }
    }).then(result => {
      res.json({
        status: "Success",
        data: result
      })
    })
  }

  async create(req, res) {
    movie.create({
      title: req.body.title,
      genre: req.body.genre,
      runtime: req.body.runtime,
      release: req.body.release,
      rated: req.body.rated,
      siteRating: req.body.siteRating,
      cast: req.body.cast,
      imdb_rating: req.body.imdb_rating,
      synopsis: req.body.synopsis,
      director: req.body.director,
      writer: req.body.writer,
      poster: req.file === undefined ? "" : req.file.filename,
      trailer: req.body.trailer
    }).then(result => {
      res.json({
        status: "Success",
        data: result
      })
    })
  }

  async update(req, res) {
    var sum = 0
    const data = await review.find({
      movie: req.body.title
    })
    for (var i = 0; i < data.length; i++) {
      sum += data[i].rating
      var ave = sum / data.length

    }

    if (req.file !== undefined) {
      var img = req.file.filename;
    }

    //    const total = rate => eval(rate.reduce((a,b) => a+b,0).toString()) / rate.length



    movie.findOneAndUpdate({
      _id: req.params.id
    }, {
      title: req.body.title,
      genre: req.body.genre,
      runtime: req.body.runtime,
      release: req.body.release,
      rated: req.body.rated,
      siteRating: ave,
      cast: req.body.cast,
      imdb_rating: req.body.imdb_rating,
      synopsis: req.body.synopsis,
      director: req.body.director,
      writer: req.body.writer,
      poster: img,
      trailer: req.body.trailer
    }).then(() => {
      return movie.findOne({
        _id: req.params.id
      })
    }).then(result => {
      res.json({
        status: "Success",
        data: result
      })
    })
  }

  async delete(req, res) {
    movie.delete({
      _id: req.params.id
    }).then(() => {
      res.json({
        status: "Success",
        data: null
      })
    })
  }

}

module.exports = new MovieController
