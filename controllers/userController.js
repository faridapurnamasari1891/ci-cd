const { user, movie } = require('../models/') // import user models
const passport = require('passport'); // import passport
const jwt = require('jsonwebtoken'); // import jsonwebtoken
const { ObjectId } = require('mongodb') // Import ObjectId from mongodb
const bcrypt=require('bcrypt')

class UserController {
  async getAll(req, res) {
    // Find all transaksi collection data
    user.find({},
    '_id username email fullname').then(result => {
      res.json({
        status: 'success',
        data: result
      })
    })
  }

  async getOne(req, res) {
    user.findOne({
      _id: req.params.id
    },'_id username email fullname').then(result => {
      res.json({
        status: "Success",
        data: result
      })
    })
  }


  // if user signup
  async signup(req, res) {
    // get the req.user from passport authentication
    const body = {
      email: req.body.email,
    };

    // create jwt token from body variable
    const token = jwt.sign({
      user: body
    }, 'secret_password')

    // success to create token
    res.status(200).json({
      message: 'Sign up success!',
      token: token
    })
  }

  // if user signin
  async signin(req, res) {
    // get the req.user from passport authentication
    const body = {
        email: req.user.email
    };

    // create jwt token from body variable
    const token = jwt.sign({
      user: body
    }, 'secret_password')

    // success to create token
    res.status(200).json({
      message: 'Sign in success!',
      token: token
    })
  }



    async update(req,res) {

      user.findOneAndUpdate({
      _id: req.params.id},
         {
        username:req.body.username,
        fullname:req.body.fullname,
        email:req.body.email,
        role:"visitor",
        password:bcrypt.hashSync(req.body.password, 10),
        profilePic: req.file===undefined?"":req.file.filename
      }).then(()=>{
        return user.findOne({
          _id:req.params.id
        })
      }).then(result=>{
        res.json({
          status:"Success updating data",
          data:result
        })
      })
    }

    async watchlist(req,res){
      const data = await Promise.all([

      movie.findOne({
          title: req.params.title
        })
      ])
      console.log(data[0].title);
      user.findOneAndUpdate({
        username: req.params.username
      }, {
        watchlist:data[0].title

      })
      .then(result=>{
      res.json({
        status:"Success updating data",
        data:result
      })
    })
    }



    async favorites(req,res){
      const data = await Promise.all([

      movie.findOne({
          _id: req.params.id
        })
      ])
      user.findOneAndUpdate({
        _id: req.body.id_user
      }, {
        favorite:data[0].title

      })
      .then(result=>{
      res.json({
        status:"Success updating data",
        data:result
      })
    })
    }

    async delete(req, res) {
      user.delete({
      _id: req.params.id
      }).then(() => {
        res.json({
          status: "success",
          data: null
        })
      })
    }


  }



module.exports = new UserController; // export UserController
