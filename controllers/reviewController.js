const {
    review
  } = require('../models');
  const passportJWT = require("passport-jwt");
  const JWTStrategy   = passportJWT.Strategy;
  const ExtractJWT = passportJWT.ExtractJwt;
  const passport=require('passport')
  const jwt = require('jsonwebtoken'); // import jsonwebtoken
  const jwt_decode = require('jwt-decode');
//  const jwttoken=require ('../middlewares/auth/index.js')


  class ReviewController {

    async getAll(req, res) {
      // Find all transaksi collection data
      review.find({}).then(result => {
        res.json({
          status: 'success',
          data: result
        })
      })
    }

      async getOneById(req, res) {
        review.findOne({
          _id: req.params.id
        }).then(result => {
          res.json({
            status: "success",
            data: result
          })
        },
      )}

      async getByMovie(req, res) {
        review.find({
          movie: {
            $regex: '.*' + req.params.movie + '.*'
          }
        }).then(result => {
          res.json({
            status: "Success",
            data: result
          })
        })
      }
      //
      async getByUser(req, res) {
        review.find({
        username: {
            $regex: '.*' + req.params.username + '.*'
          }
        }).then(result => {
          res.json({
            status: "Success",
            data: result
          })
        })
      }
      //
      async create(req, res) {
        const data = await Promise.all ([
          movie.findOne({
          title:req.params.title
        }),
        user.findOne({
          username:req.user.username
        })

      ])

        review.create({
        movie:data[0].title,
        username:data[1].username,
        review: req.body.review,
        rating: req.body.rating,

      })

    .then(result => {
          res.json({
            status: "success",
            result: result
          })
        })

    }

      async update(req, res) {

        review.findOneAndUpdate({
          _id: req.params.id
        }, {
          review: req.body.review,
          rating: req.body.rating
        }).then(() => {
          return review.findOne({
            _id: req.params.id
          })
        }).then(result => {
          res.json({
            status: "success",
            result: result
          })
        })
      }

      //
      //
      async delete(req, res) {
        review.delete({
          _id: req.params.id
        }).then(() => {
          res.json({
            status: "success",
            data: null
          })
        })
      }



  }

  module.exports = new ReviewController;
