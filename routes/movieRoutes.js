const express = require('express');
const router = express.Router();
const passport = require('passport'); // import passport
const auth = require('../middlewares/auth/index'); // import passport auth strategy
const movieValidator = require('../middlewares/validators/movieValidator');
const MovieController = require('../controllers/movieController');

router.get('/', MovieController.getAll)
router.get('/:id', movieValidator.getOne, MovieController.getOne)
router.get('/title/:title', movieValidator.getByTitle,  MovieController.getByTitle)
router.post('/create', [passport.authenticate('admin', { session: false }), movieValidator.create], MovieController.create)
router.put('/update/:id', [passport.authenticate('admin', { session: false }), movieValidator.update], MovieController.update)
router.delete('/delete/:id', [passport.authenticate('admin', { session: false }), movieValidator.delete], MovieController.delete)

module.exports = router;
