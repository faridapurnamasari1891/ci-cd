const mongoose = require("mongoose"); // Import mongoose
const mongoose_delete = require('mongoose-delete'); // Imp


var FavoriteSchema = new mongoose.Schema({
  favorite: {
    type: mongoose.Schema.Types.Mixed,
    allowNull:true,
    required:false

  },
  hasil: {
    type: mongoose.Schema.Types.Mixed,
    allowNull:true,
    required:false

  },
  {
    user:{
      type: mongoose.Schema.Types.Mixed,
      allowNull:true,
      required:false
  })

  MovieSchema.plugin(mongoose_delete, {
    overrideMethods: 'all'
  });

  module.exports = favorite = mongoose.model('favorite', FavoriteSchema, 'favorite');
