const express = require('express'); // import express
const router = express.Router(); // import router
const passport = require('passport'); // import passport
const auth = require('../middlewares/auth/index'); // import passport auth strategy
const UserController = require('../controllers/userController'); // import userController
const userValidator = require('../middlewares/validators/userValidator'); // import userValidator

// if user go to localhost:3000/signup
router.get('/', [passport.authenticate('admin', { session: false })], UserController.getAll);
router.get('/:id',[passport.authenticate('admin', { session: false })], UserController.getOne);
//router.get('/:username') ambil profile pic sama username
router.post('/signup',[ userValidator.signup,passport.authenticate( 'signup',{ session: false })], UserController.signup);
router.post('/signin', [userValidator.signin, passport.authenticate( 'signin',{ session: false })], UserController.signin);


router.put('/edit/:id',[passport.authenticate('visitor', { session: false })],userValidator.update, UserController.update)
//router.post('/:title/:username/wl',UserController.watchlist)
router.delete('/delete/:id',[passport.authenticate('admin', { session: false })],userValidator.delete , UserController.delete)

//router.put('/edit', userValidator.update, UserController.update)

module.exports = router; // export router
