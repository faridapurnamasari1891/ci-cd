const express = require('express') // Import express
const router = express.Router() // Make router from app
const passport = require('passport'); // import passport
//const auth = require('../middlewares/auth/index'); // import passport auth strategy
const FavoriteController = require('../controllers/favoriteController.js') // Import TransaksiController
//const reviewValidator = require('../middlewares/validators/reviewValidator.js') // Import validator to validate every request from user

router.post('/create', FavoriteController.create)

module.exports = router;
