const {
  review,movie,user
} = require('../../models')
const {
  check,
  validationResult,
  matchedData,
  sanitize
} = require('express-validator'); //form validation & sanitize form params


module.exports = {
  getOne: [
    check('id').custom(value => {
      return review.findOne({
        _id: value
      }).then(result => {
        if (!result) {
          throw new Error('review does not exist!')
        }
      })
    }),
    (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        });
      }
      next();
    },
  ],

  getByMovie: [
    check('movie').custom(value => {
      return review.find({
        movie: req.params.movie

      }).then(result => {

        if (result.length == 0) {
          throw new Error('movie review does not exist!')
        }
      })
    }),
  ],
  getByUser: [
    check('movie').custom(value => {
      return review.find({
        movie: req.params.movie

      }).then(result => {

        if (result.length == 0) {
          throw new Error('review by this user does not exist!')
        }
      })
    }),
  ],



  create: [
    //Set form validation rule


    check('title').custom(value => {
      return movie.findOne({
        title: value
      }).then(b => {
        if (!b) {
          throw new Error('movie does not exist!');
        }
      })
    }),
    check ('review').isString(),
    check ('username').custom(async(value, {req})=>{
    const userReview=await review.find({
      req.user.username:value
    })

    for (var i = 0; i < userReview.length; i++) {

      if (userReview[i].movie==req.params.title){
            throw new Error ('User cannot add more review')
            break;
      }
    }


  }),
//   check ('rating').custom(value=>{
//     const userRating = value
//     console.log(userRating);
//     if (!userRating===10){
//       console.log(userRating)
//     }
//   }
// ),



    (req, res, next) => {


        const errors = validationResult(req);
        if (!errors.isEmpty()) {
          return res.status(422).json({
            errors: errors.mapped()
          });
        }
        next();
      },


  ],
  update: [


    check('id').custom(value => {
      return review.findOne({
        _id: value
      }).then(b => {
        if (!b) {
          throw new Error('review does not exist');
        }

      })
    }),

  //
  // }),
    // check('title').custom(value => {
    //   return movie.findOne({
    //     title: value
    //   }).then(b => {
    //     if (!b) {
    //       throw new Error('movie does not exist!');
    //     }
    //   })
    // }),
    check ('review').isString(),

    (req, res, next) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
          return res.status(422).json({
            errors: errors.mapped()
          });
        }
        next();
      },
  ],
  delete: [
    check('id').custom(value => {
      return review.findOne({
        _id: value
      }).then(result => {
        if (!result) {
          throw new Error('ID review tidak ada!')
        }
      })
    }),
    (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        });
      }
      next();
    },
  ]
};
