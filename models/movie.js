const mongoose = require('mongoose');
const mongoose_delete = require('mongoose-delete');

const MovieSchema = new mongoose.Schema({
  title: {
    type: String,
    lowercase: true,
    required: true
  },
  genre: {
    type: mongoose.Schema.Types.Mixed,
    required: true
  },
  runtime: {
    type: String,
    required: true
  },
  release: {
    type: String,
    required: true
  },
  rated: {
    type: String,
    required: false,
    allowNull:true
  },
  siteRating:{
    type:Number,
    required:true,
    allowNull:false
  },
  cast: {
    type: String,
    required: true
  },
  imdb_rating: {
    type: Number,
    required: false
  },
  synopsis: {
    type: String,
    required: false
  },
  director: {
    type: String,
    required: false
  },
  writer: {
    type: String,
    required: false
  },
  trailer: {
    type: String,
    lowercase: true,
    default: null,
    required: false
  },
  poster: {
    type: String,
    lowercase: true,
    default: null,
    required: false
  }
}, {
  timestamps: {
    createdAt: 'created_at',
    updatedAt: 'updated_at'
  },
  versionKey: false
})

MovieSchema.path('poster').get((img) => {
  console.log(img);
  if (img === null ) {
    return '/poster/' + img
  } else {
    return '/poster/trailer.jpg'
  }
})

MovieSchema.set('toJSON', {
  getters: true
})

MovieSchema.plugin(mongoose_delete, {
  overrideMethods: 'all'
});

module.exports = movie = mongoose.model('movie', MovieSchema, 'movie');
