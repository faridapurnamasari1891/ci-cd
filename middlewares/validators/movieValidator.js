const {
  movie
} = require('../../models')
const {
  check,
  validationResult,
  matchedData,
  sanitize
} = require('express-validator'); //form validation & sanitize form params

const multer = require('multer'); //multipar form-data
const path = require('path'); // to detect path of directory
const crypto = require('crypto'); // to encrypt something

const uploadDir = '/poster/'; // make images upload to /img/
const storage = multer.diskStorage({
  destination: "./public" + uploadDir, // make images upload to /public/img/
  filename: function(req, file, cb) {
    crypto.pseudoRandomBytes(16, function(err, raw) {
      if (err) return cb(err)

      cb(null, raw.toString('hex') + path.extname(file.originalname)) // encrypt filename and save it into the /public/img/ directory
    })
  }
})

const upload = multer({
  storage: storage,
  dest: uploadDir
});

module.exports = {
  getOne: [
    check('id').custom(value => {
      return movie.findOne({
        _id: value
      }).then(result => {
        if (!result) {
          throw new Error('movie\'s ID doesn\'t exist!')
        }
      })
    }),
  ],
    getByTitle: [
      check('title').custom(value => {
        return movie.find({
          title: {
            $regex: '.*' + value + '.*'
          }
        }).then(result => {
          console.log(result.length);
          if (result.length == 0) {
            throw new Error('movie\'s doesn\'t exist!')
          }
        })
      }),
    ],

    getByGenre: [
      check('genre').custom(value => {
        return movie.find({
          genre: {
            $regex: '.*' + value + '.*'
          }
        }).then(result => {
          console.log(result.length);
          if (result.length == 0) {
            throw new Error('genre doesn\'t exist!')
          }
        })
      }),
    ],
  create: [
    //Set form validation rule
    upload.single('poster'),
    check('title').isString(),
    check('genre').isString().notEmpty(),
    (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        });
      }
      next();
    },
    check('runtime').isNumeric(),
    (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        });
      }
      next();
    },
    check('release').isString().notEmpty(),
    check('rated').isString().notEmpty(),
    check('imdb_rating').isNumeric().notEmpty(),
    check('cast').isString().notEmpty(),
    check('director').isString(),
    (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        });
      }
      next();
    },
    check('writer').isString(),
    (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        });
      }
      next();
    },
  ],
  update: [
    upload.single('poster'),
    check('id').custom(value => {
      return movie.findOne({
        _id: value
      }).then(b => {
        if (!b) {
          throw new Error('movie\'s ID not exist!');
        }
      })
    }),
    check('title').isString().notEmpty(),
    check('genre').isString().notEmpty(),
    (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        });
      }
      next();
    },
    check('runtime').isNumeric(),
    (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        });
      }
      next();
    },
    check('release').isString().notEmpty(),
    check('rated').isString().notEmpty(),
    check('imdb_rating').isNumeric().notEmpty(),
    check('cast').isString().notEmpty(),
    check('director').isString(),
    (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        });
      }
      next();
    },
    check('writer').isString(),
    (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        });
      }
      next();
    },
  ],
  delete: [
    check('id').custom(value => {
      return movie.findOne({
        _id: value
      }).then(result => {
        if (!result) {
          throw new Error('movie\'s ID not exist!')
        }
      })
    }),
    (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        });
      }
      next();
    },
  ]
};
