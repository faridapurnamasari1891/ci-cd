const express = require('express') // Import express
const app = express() // Make express app
const bodyParser = require('body-parser') // Import bodyParser
const movieRoutes=require('./routes/movieRoutes')
const userRoutes = require('./routes/userRoutes');
const reviewRoutes = require('./routes/reviewRoutes');
const favoriteRoutes = require('./routes/favoriteRoutes');


//Set body parser for HTTP post operation
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({
  extended: true
})); // support encoded bodies

//set static assets to public directory
app.use(express.static('public'));

app.use('/favorite', favoriteRoutes);
app.use('/user', userRoutes);
app.use('/movie', movieRoutes);
app.use('/review', reviewRoutes);





app.listen(6000, () => console.log('Server running on localhost:6000')) // Run server with port 3000
